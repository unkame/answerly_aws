# App Answerly (cloud)
From the book "Building-Django-2.0-Web-Applications" by Tom Aratyn

## Description
For reference of chapter 9. Majorly about implementation of "Answerly" on cloud (AWS)

- separate setting on development & production. 

- \scripts_ref\terminal_cmd.txt: terminal commands used. Executed line by line instead of a shell script

- implement on aws (free-tier)

- not mention how to create/connect instance on cloud
