from config.common_settings import *

DEBUG = True
SECRET_KEY = 'some secret'

DATABASES['default'].update = {
    'default': {
        'NAME': 'answerly',
        'USER': 'answerly',
        'PASSWORD': '0000',
        'PORT': 5432,
        'HOST': 'localhost',
    }
}

# local elastic search:
ES_INDEX = 'answerly'
ES_HOST = '192.168.99.100'
ES_PORT = '9200'

# chrome driver 
CHROMEDRIVER = os.path.join(BASE_DIR, 'chromedriver\chromedriver')

